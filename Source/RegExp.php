<?php

namespace Beecubu\Foundation\Helpers\RegExp;

use function Beecubu\Foundation\Helpers\String\removeAccents;

/**
 * Converteix una expressió regular a "accent-insensitive"
 *
 * @param string $text El text a tractar.
 *
 * @return string El text amb els accents "ignorats".
 */
function accentToRegex(string $text): string
{
    $regex =
        [
            'A' => 'AÀÁÂÃÄÅÆ',
            'C' => 'CÇ',
            'E' => 'EÈÉÊËẼ',
            'I' => 'IÌÍÎÏĨ',
            'D' => 'DÐ',
            'N' => 'NÑ',
            'O' => 'OÒÓÔÕÖØ',
            'U' => 'UÙÚÛÜ',
            'Y' => 'YÝ',
            's' => 'sß',
            'a' => 'aàáâãäåæª',
            'c' => 'cç',
            'e' => 'eèéêëẽ',
            'i' => 'iìíîïĩ',
            'o' => 'oðòóôõöø',
            'n' => 'nñ',
            'u' => 'uùúûü',
            'y' => 'yÿ',
        ];

    $text = removeAccents($text);

    foreach ($regex as $rg_key => $rg)
    {
        $text = preg_replace("/[$rg]/", "_{$rg_key}_", $text);
    }

    foreach ($regex as $rg_key => $rg)
    {
        $text = preg_replace("/_{$rg_key}_/", "[$rg]", $text);
    }

    return $text;
}