<?php

namespace Beecubu\Foundation\Helpers\HTML;

use function Beecubu\Foundation\Helpers\String\str_block_map;

/**
 * Converteix els caràcters html tipus &#039; en el seu valor "real".
 *
 * @param string $string $string El text a convertir
 * @param int $quote_style El tipus de quotes
 * @param string $charset El format de sortida
 *
 * @return string L'string sense caràcters especials.
 */
function html_entity_decode_numeric(string $string, int $quote_style = ENT_COMPAT, string $charset = 'utf-8'): string
{
    // define the chr_utf8 callback helper
    if ( ! function_exists('chr_utf8'))
    {
        function chr_utf8($num)
        {
            if ($num < 128) return chr($num);
            if ($num < 2048) return chr(($num >> 6) + 192).chr(($num & 63) + 128);
            if ($num < 65536) return chr(($num >> 12) + 224).chr((($num >> 6) & 63) + 128).chr(($num & 63) + 128);
            if ($num < 2097152) return chr(($num >> 18) + 240).chr((($num >> 12) & 63) + 128).chr((($num >> 6) & 63) + 128).chr(($num & 63) + 128);
            return '';
        }
    }
    // convert special chars
    $string = html_entity_decode($string, $quote_style, $charset);
    $string = preg_replace_callback('~&#x([0-9a-fA-F]+);~i', function ($matches)
    {
        return chr_utf8(hexdec($matches[1]));
    }, $string);
    $string = preg_replace_callback('~&#([0-9]+);~', function ($matches)
    {
        return chr_utf8($matches[1]);
    }, $string);
    return $string;
}

/**
 * Elimina tots els caràcters html especials, incloent també els numèrics (&#039;)
 *
 * @param string $html L'html a convertir.
 *
 * @return string L'html sense els caràcters especials.
 */
function decode_all_html_entities(string $html): string
{
    return html_entity_decode_numeric(html_entity_decode($html));
}

/**
 * Converteix un color hexadecimal a rgb o rgba
 *
 * @param string $color El color en format hexadecimal.
 *
 * @return string $rgb El color en format rgba (html)
 */
function hexColorToRGBA(string $color): string
{
    $opacity = null;
    $default = 'rgba(0,0,0,0)';
    //Return default if no color provided
    if (empty($color)) return $default;
    //Sanitize $color if "#" is provided
    if ($color[0] == '#')
    {
        $color = substr($color, 1);
    }

    //Check if color has 6 or 3 characters and get values
    if (strlen($color) == 8)
    {
        $hex = [$color[0].$color[1], $color[2].$color[3], $color[4].$color[5]];
        $opacity = hexdec($color[6].$color[7])/255.0;
    }
    elseif (strlen($color) == 6)
    {
        $hex = [$color[0].$color[1], $color[2].$color[3], $color[4].$color[5]];
    }
    elseif (strlen($color) == 3)
    {
        $hex = [$color[0].$color[0], $color[1].$color[1], $color[2].$color[2]];
    }
    else
    {
        return $default;
    }

    //Convert hexadec to rgb
    $rgb = array_map('hexdec', $hex);

    //Check if opacity is set(rgba or rgb)
    if ($opacity !== null)
    {
        if (abs($opacity) > 1) $opacity = 1.0;
        // create a RGB with Alpha
        $output = 'rgba('.implode(",", $rgb).','.$opacity.')';
    }
    else // standard RGB
    {
        $output = 'rgb('.implode(",", $rgb).')';
    }

    //Return rgb(a) color string
    return $output;
}

/**
 * Converteix HTML a un markdown molt simple.
 *
 * @param string $html L'html a transformar.
 *
 * @return string El text en format markdown.
 */
function htmlToSimpleMarkdown(string $html): string
{
    $text = $html;
    $text = str_replace(["\r\n", "\r", "\n"], '', $text);
    $text = str_replace('</p><p>', "\n\n", $text);
    $text = str_replace('</p><p ', "\n\n<p ", $text);
    $text = str_replace(['</p>', '<br>', '<br />', '<tr>', '<ul>', '</ul><p>'], "\n", $text);
    $text = str_replace('<ul ', "\n<ul ", $text);
    $text = preg_replace('/<b>(.*?)<\/b>/mi', '**${1}**', $text);
    $text = preg_replace('/<strong>(.*?)<\/strong>/mi', '**${1}**', $text);
    $text = preg_replace('/<i>(.*?)<\/i>/mi', '*${1}*', $text);
    $text = preg_replace('/<em>(.*?)<\/em>/mi', '*${1}*', $text);
    $text = str_replace('<li>', '■ ', $text);
    $text = str_replace('</li>', "\n", $text);
    $text = str_replace('&nbsp;', ' ', $text);
    $text = strip_tags(html_entity_decode_numeric($text));
    // trim lines
    $lines = explode("\n", $text);
    foreach ($lines as &$line)
    {
        $line = trim($line);
    }
    // return the new text
    return implode("\n", $lines);
}

/**
 * Separa en blocs el contingut HTML, per exemple, els H1, H2, H3, H4, H5, IMG, HR, A per tal de poder
 * processar-ho després dins un layout per exemple.
 *
 * @param string $html L'HTML a analitzar.
 * @param bool $convertToMarkdown TRUE = Converteix l'html a markdown, FALSE = ho retorna tal qual.
 *
 * @return array
 */
function splitHtmlIntoBreakElements(string $html, bool $convertToMarkdown): array
{
    $parts = [];
    $breaker = '--!-#-!--';
    // Prepare the breakdown...
    // A Header (H?) with link, note: All the header will be a link (ignoring the end of real link label)
    $html = preg_replace('/<\s*h\d.*?><\s*a\s.*href\s*=\s*["\'](.+?)["\'].*?>(.+?)<\s*\/\s*h\d\s*>/mi', $breaker.'[a href="${1}"]${2}[/a]'.$breaker, $html);
    // parse links
    $html = preg_replace('/<\s*a\s+(.*?)>(.*?)<\s*\/a\s*>/mi', $breaker.'[a ${1}]${2}[/a]'.$breaker, $html);
    // parse links with images
    $html = str_block_map($html, $breaker, function ($part)
    {
        return preg_replace('/\[a\s+(.*?)\]<\s*img(.*?)\/>\[\/a\]/mi', '[a ${1}][img ${2}][/a]', $part);
    });
    // process rest of html blocks
    $html = preg_replace('/<\s*hr\s*\\\\?\s*>/mi', $breaker.'[hr]'.$breaker, $html);
    $html = preg_replace('/<\s*img\s+(.*?)\/?>/mi', $breaker.'[img ${1}]'.$breaker, $html);
    $html = preg_replace('/<\s*iframe\s*(.*?)>.*<\s*\/iframe\s*>/mi', $breaker.'[iframe ${1}]'.$breaker, $html);
    $html = preg_replace('/<\s*h1\s*.*?>(.*)<\s*\/h1\s*>/mi', $breaker.'[h1]${1}[/h1]'.$breaker, $html);
    $html = preg_replace('/<\s*h2\s*.*?>(.*)<\s*\/h2\s*>/mi', $breaker.'[h2]${1}[/h2]'.$breaker, $html);
    $html = preg_replace('/<\s*h3\s*.*?>(.*)<\s*\/h3\s*>/mi', $breaker.'[h3]${1}[/h3]'.$breaker, $html);
    $html = preg_replace('/<\s*h4\s*.*?>(.*)<\s*\/h4\s*>/mi', $breaker.'[h4]${1}[/h4]'.$breaker, $html);
    $html = preg_replace('/<\s*h5\s*.*?>(.*)<\s*\/h5\s*>/mi', $breaker.'[h5]${1}[/h5]'.$breaker, $html);
    // parse each part
    foreach (explode($breaker, $html) as $index => $part)
    {
        if (empty($part))
        {
            continue;
        }
        elseif (preg_match('/\[a\s+.*href\s*=\s*[\'"](.*?)[\'"].*?\](.*)\[\/a\]/m', $part, $matches) !== 0)
        {
            $block = [
                'type' => 'link',
                'url'  => $matches[1],
            ];
            // is an image link?
            if (preg_match('/^\s*\[img\s.*src\s*=\s*["\'](.*?)["\']/mi', $matches[2], $imgMatches) !== 0)
            {
                $block['image'] = $imgMatches[1];
            }
            else // is text block (or do not start with an image)
            {
                $block['title'] = $convertToMarkdown ? htmlToSimpleMarkdown($matches[2]) : $matches[2];
            }
            $parts[] = $block;
        }
        elseif ($part === '[hr]')
        {
            $parts[] = ['type' => 'separator'];
        }
        elseif (preg_match('/\[img\s.*src=[\'"](.*?)[\'"]/mi', $part, $matches) !== 0)
        {
            $parts[] = [
                'type' => 'image',
                'url'  => $matches[1],
            ];
        }
        elseif (preg_match('/\[iframe\s.*src=[\'"](.*?)[\'"]/mi', $part, $matches) !== 0)
        {
            if (preg_match('/\[iframe\s.*title=[\'"](.*?)[\'"]/mi', $part, $titleMatches) !== 0)
            {
                $title = $titleMatches[1];
            }

            if (strpos($matches[1], 'youtube.com'))
            {
                $videoId = null;

                if (preg_match('/youtube.com\/embed\/(.*)/mi', $matches[1], $youtubeMatches) !== 0)
                {
                    $videoId = $youtubeMatches[1];
                }
                elseif (preg_match('/youtube.com\/watch?.*v=(.*?)&?/mi', $matches[1], $youtubeMatches) !== 0)
                {
                    $videoId = $youtubeMatches[1];
                }

                if ($videoId) $preview = "https://img.youtube.com/vi/$videoId/maxresdefault.jpg";
            }

            $parts[] = [
                'type'    => 'link',
                'title'   => $title ?? 'Link',
                'url'     => $matches[1],
                'preview' => $preview ?? null,
            ];
        }
        elseif (preg_match('/\[h(\d)](.*)\[\/h\d]$/m', $part, $matches) !== 0)
        {
            $parts[] = [
                'type'    => 'header',
                'level'   => (int)$matches[1],
                'content' => $convertToMarkdown ? htmlToSimpleMarkdown($matches[2]) : $matches[2],
            ];
        }
        else // text block
        {
            $text = $convertToMarkdown ? htmlToSimpleMarkdown($part) : $part;
            // ignore empty spaces
            if ($index === 0 && empty($text)) continue;
            if (in_array($text, ['**', '*', '.'])) continue;
            // add part
            $parts[] = [
                'type'    => 'text',
                'content' => $text,
            ];
        }
    }
    return $parts;
}
