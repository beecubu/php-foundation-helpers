<?php

namespace Beecubu\Foundation\Helpers\Id;

use Exception;

/**
 * Genera un identificador únic de 12 caràcters.
 * Aquest mètode utilitza internament el uniqid del php però convertint-lo a una versió de 12 caràcters.
 * <b>Important:</b> La longitud mínima és de 10 caràcters si es volen ids realment únics.
 *
 * @param int $length La longitud de l'id.
 *
 * @return string
 */
function uniqid(int $length = 12): string
{
    $id = '';
    // generate a random id
    while (strlen($id) < $length)
    {
        $str = str_replace('.', '', \uniqid('', true));
        $id .= base_convert($str, 16, 36);
    }
    return substr($id, -$length, $length);
}

/**
 * Genera un uuid v4.
 *
 * @param bool $compact TRUE = Retorna l'uuid sense els '-' (32 bytes), FALSE = el uuid normal (36 bytes)
 *
 * @return string El UUID generat.
 *
 * @throws Exception
 */
function uuid(bool $compact = false): ?string
{
    try
    {
        $data = random_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
        // create the uuid
        return vsprintf($compact ? '%s%s%s%s%s%s%s%s' : '%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
    catch (Exception $exception)
    {
        return null;
    }
}

/**
 * Comprova que sigui un Id vàlid pel mongo.
 *
 * @param string $id L'id a provar.
 *
 * @return TRUE = Tot ok, FALSE = no és vàlid,
 */
function validateObjectId(string $id): bool
{
    return strlen($id) === 24 && strspn($id, '0123456789ABCDEFabcdef') === 24;
}