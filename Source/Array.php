<?php

namespace Beecubu\Foundation\Helpers\Arrays;

use stdClass;

/**
 * Mou un element d'un array a una altre posició.
 *
 * @param array $array L'array que es vol modificar.
 * @param int $from La posició que es vol moure.
 * @param int $to La posició on es vol moure.
 */
function array_move_item(array &$array, int $from, int $to): void
{
    $out = array_splice($array, $from, 1);
    array_splice($array, $to, 0, $out);
}

/**
 * S'assegura d'afegir o excloure un valor dins l'array.
 *
 * @param array $array L'array que es vol modificar.
 * @param mixed $value El valor a incloure o excloure.
 * @param bool $include TRUE = S'assegura que el valor estigui dins l'array i l'afegeix si fa falta, FALSE = s'assegura que el valor no hi sigui, i el treu si fa falta.
 *
 * @return void
 */
function array_ensure_value(array &$array, $value, bool $include): void
{
    $index = array_search($value, $array);
    // if excluding and the value is not found, add it
    if ($include && $index === false)
    {
        $array[] = $value;
    }
    elseif ( ! $include && $index !== false)
    {
        unset($array[$index]);
    }
}

/**
 * Ordena un array (CSV per exemple) per una o mes columnes.
 *
 * @param array $array L'array d'entrada.
 * @param array $columns Les columnes a ordenar.
 */
function sortArrayByColumns(array &$array, array $columns): void
{
    usort($array, function ($a, $b) use ($columns)
    {
        $cmp = 0;
        // sort taking care about each column
        foreach ($columns as $column)
        {
            $cmp = $a[$column] <=> $b[$column];
            // is not equal?
            if ($cmp != 0) return $cmp;
        }
        // return the comparision result
        return $cmp;
    });
}

/**
 * Converteix un array (tipus taula) a html.
 *
 * @param array $array L'array a processar.
 * @param bool $header TRUE = La primera fila es considera capçalera, FALSE = tot és taula.
 *
 * @return string L'html.
 */
function array2html(array $array, bool $header = false): string
{
    $html = '<table style="text-align:right;">';
    foreach ($array as $row)
    {
        $html .= '<tr>';
        $td = $header ? 'th' : 'td';
        foreach ($row as $cell)
        {
            if ( ! is_string($cell) && ! is_numeric($cell)) $cell = '';
            // generate cell html
            $html .= "<$td style='border: 1px solid #dddddd;padding: 4px;'>".stripslashes($cell)."</$td>";
        }
        $header = false;
        $html .= '</tr>';
    }
    $html .= '</table>';
    return $html;
}

/**
 * Converteix un stdClass a array.
 *
 * @param array|stdClass $value El valor a convertir.
 *
 * @return mixed L'objecte stdClass convertit a array.
 */
function stdClass2Array($value)
{
    if ($value instanceof stdClass)
    {
        $value = get_object_vars($value);
    }
    // parse the array
    if (is_array($value))
    {
        return array_map(__FUNCTION__, $value);
    }
    else // return as is...
    {
        return $value;
    }
}

/**
 * Obté un valor d'un array o stdClass.
 *
 * @param array|stdClass $values Els valors.
 * @param string $key El valor a obtenir.
 *
 * @return mixed
 */
function valueFromArrayOrStdClass($values, string $key, $default = null)
{
    return is_object($values) ? ($values->$key ?? $default) : ($values[$key] ?? $default);
}
