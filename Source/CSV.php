<?php

namespace Beecubu\Foundation\Helpers\CSV;

use DateTime;

/**
 * Comprova un fitxer CSV línia per línia (auto-detecta el format del CSV).
 *
 * @param string $file El fitxer CSV a comprovar.
 * @param callable $callback El mètode a cridar per cada línia "function(array $values)".
 */
function parse_csv(string $file, callable $callback): void
{
    // auto-detect the configuration to use
    $config = detectCsvConfig($file);
    // parse the CSV using an auto-detected configuration
    parse_csv_with_config($file, $config['columnSeparator'], $config['quoteCharacter'], $callback);
}

/**
 * Neteja els caràcters estranys d'un csv i el torna a guardar. (\r\n -> \n)
 *
 * @param string $file El fitxer CSV a netejar.
 */
function clean_csv(string $file): void
{
    file_put_contents($file, str_replace(chr(226).chr(128).chr(168), "\n", file_get_contents($file)));
}

/**
 * Neteja i Comprova un fitxer CSV línia per línia (auto-detecta el format del CSV).
 *
 * @param string $file El fitxer CSV a netejar i comprovar.
 * @param callable $callback El mètode a cridar per cada línia "function($values)".
 */
function clean_and_parse_csv(string $file, callable $callback): void
{
    // clean csv
    clean_csv($file);
    // auto-detect the configuration to use
    $config = detectCsvConfig($file);
    // parse the CSV using an auto-detected configuration
    parse_csv_with_config($file, $config['columnSeparator'], $config['quoteCharacter'], $callback);
}

/**
 * Comprova un fitxer CSV lina per línia utilitzant una configuració concreta.
 *
 * @param string $file El fitxer CSV a comprovar.
 * @param string $columnSeparator El caràcter de separador entre columnes.
 * @param string $quoteCharacter El caràcter per encapsular els strings.
 * @param callable $callback El mètode a cridar per cada línia "function($values)".
 */
function parse_csv_with_config(string $file, string $columnSeparator, string $quoteCharacter, callable $callback): void
{
    $headers = null;
    // open the csv file
    if (($handle = fopen($file, 'r')) !== false)
    {
        $stop = false;
        // parse
        while (($data = fgetcsv($handle, null, $columnSeparator, $quoteCharacter)) !== false && $stop === false)
        {
            if ($headers)
            {
                if ($values = @array_combine($headers, $data))
                {
                    $callback($values, $stop);
                }
            }
            else // create the headers list
            {
                $headers = $data;
            }
        }
    }
}

/**
 * Intenta detectar la configuració d'un CSV.
 *
 * @param string $file El fitxer CSV a comprovar.
 *
 * @return array La configuració detectada.
 */
function detectCsvConfig(string $file): array
{
    $configs =
        [
            ['columnSeparator' => ',', 'quoteCharacter' => '"'],
            ['columnSeparator' => ';', 'quoteCharacter' => '"'],
            ['columnSeparator' => "\t", 'quoteCharacter' => '"'],
        ];
    // init some vars
    $bestScore = 0;
    $bestConfig = null;
    // try to parse using different configurations
    foreach ($configs as $configIdx => $config)
    {
        $rowsCount = 0;
        $headerCount = 0;
        $rows = [];
        $score = 0;
        // parsed lines
        parse_csv_with_config($file, $config['columnSeparator'], $config['quoteCharacter'], function ($row, &$stop) use (&$rows, &$rowsCount, &$headerCount)
        {
            if ($rowsCount === 0)
            {
                $headerCount = count($row);
            }
            // add this new row
            $rows[] = $row;
            $rowsCount++;
            // read only the first five lines
            $stop = $rowsCount === 5;
        });
        // calcule the score
        $score += count($rows);
        // sub-score calculation
        $subScore = 0;
        // analise each row
        foreach ($rows as $row)
        {
            if ($headerCount === count($row))
            {
                $subScore += $headerCount;
            }
        }
        // is the expected count equals to real count? then +5
        if (count($rows)*$headerCount === $subScore)
        {
            $subScore += 5;
        }
        // add the sub-score
        $score += $subScore;
        // is it the best config from now?
        if ($score > $bestScore)
        {
            $bestScore = $score;
            $bestConfig = $config;
        }
    }
    // return the winner
    return $bestConfig;
}

/**
 * Carrega una CSV a memòria.
 *
 * @param string $file El fitxer CSV a comprovar.
 *
 * @return array El CSV.
 */
function load_csv(string $file): array
{
    $values = [];
    // load csv
    parse_csv($file, function ($line) use (&$values)
    {
        $values[] = $line;
    });
    // return the array
    return $values;
}

/**
 * Converteix un array a CSV.
 *
 * @param array $array El dataSet.
 * @param bool $automaticColumns TRUE = Afegeix els noms de les columnes, FALSE = no fa res.
 * @param string $columnSeparator El caràcter de separador entre columnes.
 * @param string $decimalMark El caràcter que es considera decimal.
 * @param string $quoteCharacter El caràcter per encapsular els strings.
 * @param string $dateFormat El format a utilitzar a la hora de convertir les dates.
 *
 * @return string El CSV en format text.
 */
function array2csv(array $array, $automaticColumns = false, $columnSeparator = ',', $decimalMark = '.', $quoteCharacter = '"', $dateFormat = 'Y-m-d h:i:s')
{
    $columns = [];
    // normalize the dataSet
    foreach ($array as $row)
    {
        $columns = array_unique(array_merge($columns, array_keys($row)));
    }
    // inits
    $csv = [];
    // add the header row
    if ($automaticColumns)
    {
        $columnValues = $columns;
        array_walk($columnValues, function (&$value) use ($quoteCharacter)
        {
            $value = $quoteCharacter.addslashes($value).$quoteCharacter;
        });
        // append the columns row
        $csv[] = implode($columnSeparator, array_combine($columns, $columnValues));
    }
    // parse and generate CSV
    foreach ($array as $items)
    {
        $row = [];
        // build each row using the columns template
        foreach ($columns as $column)
        {
            $cell = '';
            // is the value present?
            if (isset($items[$column]))
            {
                $cell = $items[$column];
                // is an object
                if (is_object($cell) && $cell instanceof DateTime) $cell = $cell->format($dateFormat);
                // convert
                if (is_string($cell)) $cell = $quoteCharacter.addslashes($cell).$quoteCharacter;
                elseif (is_numeric($cell)) $cell = str_replace(['.', ','], $decimalMark, $cell);
                else $cell = '';
            }
            // set column
            $row[$column] = $cell;
        }
        // add csv row
        $csv[] = implode($columnSeparator, $row);
    }
    // the CSV
    return implode("\n", $csv);
}
