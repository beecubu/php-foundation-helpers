<?php

namespace Beecubu\Foundation\Helpers\Phone;

use function Beecubu\Foundation\Helpers\String\parseInt;

/**
 * Posa maco un número de telèfon, seguin un estàndard de grups de 3 (Exemple: +xx xxx xxx xxx)
 *
 * @param string $value El numero de telèfon a posar maco.
 * @param string $countryCode El codi del país (opcional).
 *
 * @return string El número de telèfon posat maco.
 */
function phoneNumberFormatter(string $value, ?string $countryCode = null): string
{
    $result = strrev(implode(' ', str_split(strrev(phoneNumberParser($value)), 3)));
    // has a country code?
    if ($countryCode) $result = "(+$countryCode) $result";
    // the phone number formatted
    return $result;
}

/**
 * Comprova el número de telèfon, per tal de que quedi net, sense espaís ni lletres invàlides, només s'accepta el +
 * davant de tot del número de telèfon.
 *
 * @param string $value El numero de telèfon a comprovar.
 *
 * @return string El numero de telèfon comprovat (net, sense coses estranyes que no siguin part del num. telèfon).
 */
function phoneNumberParser(string $value): string
{
    $plus = strpos(trim($value), '+') === 0 ? '+' : '';
    // remove invalid chars
    $value = parseInt($value);
    // return the name parsed
    return $plus.$value;
}