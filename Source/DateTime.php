<?php

namespace Beecubu\Foundation\Helpers\DateTime;

use DateTime;
use Exception;

/**
 * Compara dos objectes del tipus DateTime (però només compara les dates i ignora l'hora, minuts i segons) i retorna quina data és més gran
 *
 * @param DateTime $date1 La primera data a comparar
 * @param DateTime $date2 La segona data a comparar (opcional). Si es deixa en NULL llavors ho compara amb la data actual.
 *
 * @return int Més petit de 0, la data 1 és més vella, 0 són iguals i més gran de 0 la data 2 és més vella.
 *
 * @throws
 */
function compareDates(DateTime $date1, ?DateTime $date2 = null): int
{
    if ( ! $date2)
    {
        $date2 = new DateTime();
    }
    // compare dates
    return strcmp($date1->format('Y-m-d'), $date2->format('Y-m-d'));
}

/**
 * Compara dos objectes del tipus DateTime (però només compara les dates i ignora l'hora, minuts i segons).
 *
 * @param DateTime $date1 La primera data a comparar
 * @param DateTime $date2 La segona data a comparar
 *
 * @return bool TRUE = Són iguals, FALSE = no.
 */
function datesAreEqual(DateTime $date1, DateTime $date2): bool
{
    return compareDates($date1, $date2) == 0;
}

/**
 * Comprova si una data és avui.
 *
 * @param DateTime $date La data a comprovar
 *
 * @return bool TRUE = La data és avui, FALSE = no.
 *
 * @throws
 */
function dateIsToday(DateTime $date): bool
{
    return datesAreEqual($date, new DateTime());
}

/**
 * Comprova si una data és més vella que avui.
 *
 * @param DateTime $date La data a comprovar
 *
 * @return bool TRUE = La data és avui, FALSE = no.
 */
function dateIsOlderThanToday(DateTime $date): bool
{
    return compareDates($date) < 0;
}

/**
 * Comprova si una data és avui o més vella que avui.
 *
 * @param DateTime $date La data a comprovar
 *
 * @return bool TRUE = La data és avui o més vella, FALSE = no.
 */
function dateIsEqualOrOlderThanToday(DateTime $date): bool
{
    return compareDates($date) <= 0;
}

/**
 * Comprova si una data és futura (respecte avui).
 *
 * @param DateTime $date La data a comprovar
 *
 * @return bool TRUE = La data és futura respecte avui, FALSE = no.
 */
function dateIsAFutureDate(DateTime $date): bool
{
    return compareDates($date) > 0;
}

/**
 * Comprova si una data és avui o futura (respecte avui).
 *
 * @param DateTime $date La data a comprovar
 *
 * @return bool TRUE = La data és avui o futura respecte avui, FALSE = no.
 */
function dateIsTodayOrAFutureDate(DateTime $date): bool
{
    return compareDates($date) >= 0;
}

/**
 * Retorna una data al començament del dia, és a dir, hores, minuts i segons a 0.
 *
 * @param DateTime $date La data a convertir.
 *
 * @return DateTime La data al principi del dia.
 */
function dateAsStartOfDay(DateTime $date): DateTime
{
    $new = clone $date;
    $new->setTime(0, 0, 0);
    return $new;
}

/**
 * Retorna una data al final del dia, és a dir, hores, minuts i segons a 23:59:59.
 *
 * @param DateTime $date La data a convertir.
 *
 * @return DateTime La nova data posada al final del dia.
 */
function dateAsEndOfDay(DateTime $date): DateTime
{
    $new = clone $date;
    $new->setTime(23, 59, 59);
    return $new;
}

/**
 * Retorna la data en mil·lisegons.
 *
 * @param DateTime $date La data.
 *
 * @return float La data en mil·lisegons.
 */
function dateToMilliseconds(DateTime $date): float
{
    return (float)$date->format('Uv');
}

/**
 * Obté informació detallada d'una Data i Hora.
 *
 * @param DateTime $dateTime La data de la que es vol informació.
 *
 * @return array La informació.
 */
function dateTimeInformation(DateTime $dateTime): array
{
    $result = [];
    // date information
    $result['year'] = (int)$dateTime->format('Y');
    $result['month'] = (int)$dateTime->format('m');
    $result['day'] = (int)$dateTime->format('d');
    $result['day_week'] = (int)$dateTime->format('N');
    $result['day_year'] = (int)$dateTime->format('z');
    // time information
    $result['hour'] = (int)$dateTime->format('H');
    $result['minutes'] = (int)$dateTime->format('i');
    $result['seconds'] = (int)$dateTime->format('s');
    // the information
    return $result;
}

/**
 * Converteix una data a una forma més humana i natural.
 *
 * @param DateTime $date La data a convertir.
 * @param bool $allDay TRUE = Si es considera que es tot el dia, FALSE = és a una hora concreta.
 * @param string $locale L'idioma en el que es vol (ca, es, en)
 *
 * @return string La data en format humà.
 *
 * @throws Exception
 */
function dateTimeToHumanWriting(DateTime $date, bool $allDay, string $locale): string
{
    return dateTimeIntervalToHumanWriting($date, $date, $allDay, $locale);
}

/**
 * Converteix un interval de dates a una forma més humana i natural.
 *
 * @param DateTime $start La data d'inici a convertir.
 * @param DateTime $end La data de finalització a convertir.
 * @param bool $allDay TRUE = Si es considera que es tot el dia, FALSE = és a una hora concreta.
 * @param string $locale L'idioma en el que es vol (ca, es, en)
 *
 * @return string L'interval en format humà.
 *
 * @throws Exception
 */
function dateTimeIntervalToHumanWriting(DateTime $start, DateTime $end, bool $allDay, string $locale): string
{
    $startDateInfo = dateTimeInformation($start);
    $endDateInfo = dateTimeInformation($end);

    static $dayNames = [
        'ca' => [1 => 'dilluns', 'dimarts', 'dimecres', 'dijous', 'divendres', 'dissabte', 'diumenge'],
        'es' => [1 => 'lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado', 'domingo'],
    ];

    static $monthNames = [
        'ca' => [1 => 'de gener', 'de febrer', 'de març', 'd\'abril', 'de maig', 'de juny', 'de juliol', 'd\'agost', 'de setembre', 'd\'octubre', 'de novembre', 'de desembre'],
        'es' => [1 => 'de enero', 'de febrero', 'de marzo', 'de abril', 'de mayo', 'de junio', 'de julio', 'de agosto', 'de septiembre', 'de octubre', 'de noviembre', 'de diciembre'],
    ];

    if ($allDay)
    {
        if (datesAreEqual($start, $end))
        {
            switch ($locale)
            {
                case 'ca':
                    return 'El '.$dayNames[$locale][$startDateInfo['day_week']].' '.$startDateInfo['day'].' '.$monthNames[$locale][$startDateInfo['month']].' del '.$startDateInfo['year'].' tot el dia';

                case 'es':
                    return 'El '.$dayNames[$locale][$startDateInfo['day_week']].' '.$startDateInfo['day'].' '.$monthNames[$locale][$startDateInfo['month']].' de '.$startDateInfo['year'].' todo el dia';
            }
        }
        else // varios dies...
        {
            if ($startDateInfo['year'] !== $endDateInfo['year'])
            {
                switch ($locale)
                {
                    case 'ca':
                        return 'Tots els dies des del '.$dayNames[$locale][$startDateInfo['day_week']].' '.$startDateInfo['day'].' '.$monthNames[$locale][$startDateInfo['month']].' del '.$startDateInfo['year'].' al '.
                            $dayNames[$locale][$endDateInfo['day_week']].' '.$endDateInfo['day'].' '.$monthNames[$locale][$endDateInfo['month']].' del '.$endDateInfo['year'];

                    case 'es':
                        return 'Todos los dias desde el '.$dayNames[$locale][$startDateInfo['day_week']].' '.$startDateInfo['day'].' '.$monthNames[$locale][$startDateInfo['month']].' de '.$startDateInfo['year'].' al '.
                            $dayNames[$locale][$endDateInfo['day_week']].' '.$endDateInfo['day'].' '.$monthNames[$locale][$endDateInfo['month']].' de '.$endDateInfo['year'];
                }
            }
            elseif ($startDateInfo['month'] !== $endDateInfo['month'])
            {
                switch ($locale)
                {
                    case 'ca':
                        return 'Tots els dies des del '.$dayNames[$locale][$startDateInfo['day_week']].' '.$startDateInfo['day'].' '.$monthNames[$locale][$startDateInfo['month']].' al '.
                            $dayNames[$locale][$endDateInfo['day_week']].' '.$endDateInfo['day'].' '.$monthNames[$locale][$endDateInfo['month']].' del '.$endDateInfo['year'];

                    case 'es':
                        return 'Todos los dias desde el '.$dayNames[$locale][$startDateInfo['day_week']].' '.$startDateInfo['day'].' '.$monthNames[$locale][$startDateInfo['month']].' al '.
                            $dayNames[$locale][$endDateInfo['day_week']].' '.$endDateInfo['day'].' '.$monthNames[$locale][$endDateInfo['month']].' de '.$endDateInfo['year'];
                }
            }
            elseif ($endDateInfo['day_year'] - $startDateInfo['day_year'] === 1)
            {
                switch ($locale)
                {
                    case 'ca':
                        return 'El '.$dayNames[$locale][$startDateInfo['day_week']].' '.$startDateInfo['day'].' i el '.$dayNames[$locale][$endDateInfo['day_week']].' '.$endDateInfo['day'].' '.
                            $monthNames[$locale][$startDateInfo['month']].' del '.$startDateInfo['year'];

                    case 'es':
                        return 'El '.$dayNames[$locale][$startDateInfo['day_week']].' '.$startDateInfo['day'].' y el '.$dayNames[$locale][$endDateInfo['day_week']].' '.$endDateInfo['day'].' '.
                            $monthNames[$locale][$startDateInfo['month']].' de '.$startDateInfo['year'];
                }
            }
            else // mes de 2 dies seguits...
            {
                switch ($locale)
                {
                    case 'ca':
                        return 'Des del '.$dayNames[$locale][$startDateInfo['day_week']].' '.$startDateInfo['day'].' al '.$dayNames[$locale][$endDateInfo['day_week']].' '.$endDateInfo['day'].' '.
                            $monthNames[$locale][$startDateInfo['month']].' del '.$startDateInfo['year'].' tots els dies';

                    case 'es':
                        return 'Desde el '.$dayNames[$locale][$startDateInfo['day_week']].' '.$startDateInfo['day'].' al '.$dayNames[$locale][$endDateInfo['day_week']].' '.$endDateInfo['day'].' '.
                            $monthNames[$locale][$startDateInfo['month']].' de '.$startDateInfo['year'].' todos los dias';
                }
            }
        }
    }
    else // un dia, entre dues hores
    {
        if ($start->getTimestamp() === $end->getTimestamp())
        {
            switch ($locale)
            {
                case 'ca':
                    return 'El '.$dayNames[$locale][$startDateInfo['day_week']].' '.$startDateInfo['day'].' '.$monthNames[$locale][$startDateInfo['month']].' del '.$startDateInfo['year'].' a les '.$start->format('H:i');

                case 'es':
                    return 'El '.$dayNames[$locale][$startDateInfo['day_week']].' '.$startDateInfo['day'].' '.$monthNames[$locale][$startDateInfo['month']].' de '.$startDateInfo['year'].' a las '.$start->format('H:i');
            }
        }
        else // les dates son diferents
        {
            if (datesAreEqual($start, $end))
            {
                switch ($locale)
                {
                    case 'ca':
                        return 'El '.$dayNames[$locale][$startDateInfo['day_week']].' '.$startDateInfo['day'].' '.$monthNames[$locale][$startDateInfo['month']].' del '.$startDateInfo['year'].' des de les '.
                            $start->format('H:i').' fins a les '.$end->format('H:i').' del mateix dia';

                    case 'es':
                        return 'El '.$dayNames[$locale][$startDateInfo['day_week']].' '.$startDateInfo['day'].' '.$monthNames[$locale][$startDateInfo['month']].' de '.$startDateInfo['year'].' desde las '.
                            $start->format('H:i').' hasta las '.$end->format('H:i').' de el mismo dia';
                }
            }
            elseif ($startDateInfo['year'] !== $endDateInfo['year'])
            {
                switch ($locale)
                {
                    case 'ca':
                        return 'Des del '.$dayNames[$locale][$startDateInfo['day_week']].' '.$startDateInfo['day'].' '.$monthNames[$locale][$startDateInfo['month']].' del '.$startDateInfo['year'].' a les '.$start->format('H:i').
                            ' fins a les '.$end->format('H:i').' del '.$dayNames[$locale][$endDateInfo['day_week']].' '.$endDateInfo['day'].' '.$monthNames[$locale][$endDateInfo['month']].' del '.$endDateInfo['year'];

                    case 'es':
                        return 'Desde el '.$dayNames[$locale][$startDateInfo['day_week']].' '.$startDateInfo['day'].' '.$monthNames[$locale][$startDateInfo['month']].' de '.$startDateInfo['year'].' a las '.$start->format('H:i').
                            ' hasta las '.$end->format('H:i').' de el '.$dayNames[$locale][$endDateInfo['day_week']].' '.$endDateInfo['day'].' '.$monthNames[$locale][$endDateInfo['month']].' de '.$endDateInfo['year'];
                }
            }
            elseif ($startDateInfo['month'] !== $endDateInfo['month'])
            {
                switch ($locale)
                {
                    case 'ca':
                        return 'Des del '.$dayNames[$locale][$startDateInfo['day_week']].' '.$startDateInfo['day'].' '.$monthNames[$locale][$startDateInfo['month']].' a les '.$start->format('H:i').
                            ' fins a les '.$end->format('H:i').' del '.$dayNames[$locale][$endDateInfo['day_week']].' '.$endDateInfo['day'].' '.$monthNames[$locale][$endDateInfo['month']].' del '.$endDateInfo['year'];

                    case 'es':
                        return 'Desde el '.$dayNames[$locale][$startDateInfo['day_week']].' '.$startDateInfo['day'].' '.$monthNames[$locale][$startDateInfo['month']].' a las '.$start->format('H:i').
                            ' hasta las '.$end->format('H:i').' de el '.$dayNames[$locale][$endDateInfo['day_week']].' '.$endDateInfo['day'].' '.$monthNames[$locale][$endDateInfo['month']].' del '.$endDateInfo['year'];
                }
            }
            else // diferent days
            {
                switch ($locale)
                {
                    case 'ca':
                        return 'Des del '.$dayNames[$locale][$startDateInfo['day_week']].' '.$startDateInfo['day'].' '.$monthNames[$locale][$startDateInfo['month']].' del '.$endDateInfo['year'].' a les '.$start->format('H:i').
                            ' fins al '.$dayNames[$locale][$endDateInfo['day_week']].' '.$endDateInfo['day'].' a les '.$end->format('H:i');

                    case 'es':
                        return 'Desde el '.$dayNames[$locale][$startDateInfo['day_week']].' '.$startDateInfo['day'].' '.$monthNames[$locale][$startDateInfo['month']].' de '.$endDateInfo['year'].' a las '.$start->format('H:i').
                            ' hasta el '.$dayNames[$locale][$endDateInfo['day_week']].' '.$endDateInfo['day'].' a las '.$end->format('H:i');
                }
            }
        }
    }
    return '';
}

/**
 * Crea un DateTime a partir d'un string.
 *
 * @param string|null $dateString La data en format text.
 *
 * @return DateTime|null
 */
function dateFromString(?string $dateString): ?DateTime
{
    $formats = [
        'Y-m-d\TH:i',
        'Y-m-d',
        'd/m/Y',
        'd/m/Y H:i:s',
        'd/m/Y H:i',
        'd/m/Y, H:i',
        'Y-m-d H:i:s',
    ];

    foreach ($formats as $format) {
        $date = DateTime::createFromFormat($format, $dateString);
        if ($date !== false) {
            return $date;
        }
    }

    // If a DateTime object couldn't be created, you can handle the error or return null
    return null;
}
