<?php

namespace Beecubu\Foundation\Helpers\String;

/**
 * Elimina tots els espaís del text.
 *
 * @param string $value El text a tractar.
 *
 * @return string El text sense espaís.
 */
function trim_all(string $value): string
{
    return preg_replace('/\s+/', '', $value);
}

/**
 * Elimina tots els accents i caràcters i altres com les ç
 *
 * @param string $text El text a tractar.
 *
 * @return string El text sense accents.
 */
function removeAccents(string $text): string
{
    return transliterator_transliterate('Any-Latin; Latin-ASCII; [\u0080-\u7fff] remove', $text);
}

/**
 * És la funció str_pad però unicode.
 *
 * @param string $str El text a modificar.
 * @param int $pad_len La longitud del pad.
 * @param string $pad_str El text que s'utilitzarà com a pad.
 * @param int $dir La direcció del pad.
 * @param string|null $encoding La codificació del text.
 *
 * @return string
 */
function mb_str_pad(string $str, int $pad_len, string $pad_str = ' ', int $dir = STR_PAD_RIGHT, ?string $encoding = null): string
{
    $encoding = $encoding === null ? mb_internal_encoding() : $encoding;
    $padBefore = $dir === STR_PAD_BOTH || $dir === STR_PAD_LEFT;
    $padAfter = $dir === STR_PAD_BOTH || $dir === STR_PAD_RIGHT;
    $pad_len -= mb_strlen($str, $encoding);
    $targetLen = $padBefore && $padAfter ? $pad_len/2 : $pad_len;
    $strToRepeatLen = mb_strlen($pad_str, $encoding);
    $repeatTimes = ceil($targetLen/$strToRepeatLen);
    $repeatedString = str_repeat($pad_str, max(0, $repeatTimes)); // safe if used with valid unicode sequences (any charset)
    $before = $padBefore ? mb_substr($repeatedString, 0, floor($targetLen), $encoding) : '';
    $after = $padAfter ? mb_substr($repeatedString, 0, ceil($targetLen), $encoding) : '';
    return $before.$str.$after;
}

/**
 * És la funció ucfrist però unicode.
 *
 * @param string $string El text a convertir.
 * @param string|null $encoding La codificació del text.
 *
 * @return string
 */
function mb_ucfirst(string $string, ?string $encoding = null): string
{
    $encoding = $encoding === null ? mb_internal_encoding() : $encoding;
    $firstChar = mb_strtoupper(mb_substr($string, 0, 1, $encoding), $encoding);
    $rest = mb_strtolower(mb_substr($string, 1, mb_strlen($string, $encoding) - 1, $encoding));
    return $firstChar.$rest;
}

/**
 * És la funció ucwords però unicode.
 *
 * @param string $string El text a convertir.
 * @param string|null $encoding La codificació del text.
 *
 * @return string
 */
function mb_ucwords(string $string, ?string $encoding = null): string
{
    $words = mb_split('\s', $string);
    $words = array_map(function ($word) use ($encoding)
    {
        return mb_ucfirst($word, $encoding);
    }, $words);
    return implode(' ', $words);
}

/**
 * Codifica en base64 en un format compatible amb les URLS.
 *
 * https://simplycalc.com/base64url-encode.php
 *
 * @param string $data La informació a codificar.
 *
 * @return string La informació en base64 (url safe)
 */
function base64url_encode(string $data): string
{
    return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
}

/**
 * Descodifica un bas64 que és compatible amb les URLS.
 *
 * https://simplycalc.com/base64url-encode.php
 *
 * @param string $data La informació a descodificar.
 *
 * @return string|false La informació descodificada.
 */
function base64url_decode(string $data)
{
    return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data)%4, '=', STR_PAD_RIGHT));
}

/**
 * S'assegura que només conté números.
 *
 * @param string $string El número a revisar.
 *
 * @return string El número.
 */
function parseInt(string $string): string
{
    return preg_replace('/[^0-9]/', '', $string);
}

/**
 * Separa un text en trossos separats per un "separador", on cada tros es pot "modificar". A l'acabar
 * retorna l'string original modificat, mantenint els "separadors".
 *
 * @param string $string El text a tractar.
 * @param string $separator El separador.
 * @param callable $modifier El modifier: function (string $text): string
 *
 * @return string
 */
function str_block_map(string $string, string $separator, callable $modifier): string
{
    $parts = explode($separator, $string);
    // perform modifier to each part
    foreach ($parts as &$part)
    {
        if ($modifier) $part = $modifier($part);
    }
    // the modified string
    return implode($separator, $parts);
}

/**
 * Afegeix al final d'un string un altre string unit per "union".
 *
 * @param string|null $string El text original
 * @param string|null $append El text a afegir
 * @param string $union La unió.
 *
 * @return string|null
 */
function strAppend(?string $string, ?string $append, string $union = '.'): ?string
{
    if ( ! $string && ! $append) return null;
    if ($string && ! $append) return $string;
    if ( ! $string && $append) return $append;
    return $string.$union.$append;
}
