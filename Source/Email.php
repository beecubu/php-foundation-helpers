<?php

namespace Beecubu\Foundation\Helpers\Email;

/**
 * Parseja i valida que un email sigui correcte.
 *
 * @param string $email L'email que es vol validar.
 *
 * @return boolean TRUE = És vàlid, FALSE = no.
 */
function isValid(string $email): bool
{
    return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
}