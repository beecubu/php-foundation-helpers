1.10.0
------
- [String] Afegit nou mètode **mb_ucfirst** per posar en majúscula la primera lletra.
- [String] Afegit nou mètode **mb_ucwords** per posar en majúscula cada paraula del text.

1.9.0
-----
- [Array] Afegida nova funció **array_ensure_value** per assegurar-se que un valor estigui present, o que el valor no hi sigui.

1.8.0
-----
- [String] Afegit nou mètode **strAppend** per afegir al final amb un "union"

1.7.0
-----
- [DateTime] Afegit un nou mètode **dateFromString** per crear un DateTime a partir d'un string.
- [HTML] Millorat el mètode **htmlToSimpleMarkdown**.
- [HTML] Afegit un nou mètode **splitHtmlIntoBreakElements** per separar un html a trossos.
- [String] Afegit un nou mètode **str_block_map** per separar un string per tokens.

1.6.1
-----
- [Array] Solucionat bug amb el stdClass2Array()

1.6.0
-----
- [Array] Afegit un nou mètode **valueFromArrayOrStdClass** per obtenir un valor d'un array o stdClass.

1.5.1
-----
- [CSV] Millorada la conversió d'array a CSV (array2csv)

1.5.0
-----
- [RegExp] Solucionat bug al **accentToRegex**.
- [String] Afegit un nou mètode: **parseInt**.

1.4.1
-----
- [String] Solucionat bug amb la funció **base64url_decode** que no tenia en compte que si falla retorna **false**.

1.4.0
-----
- [String] Afegits dos nous mètodes per codificar i descodificar base64 compatibles amb les URLs.

1.3.0
-----
- [HTML] Afegit un nou Helper per convertir HTML a MarkDown
    - El mètode **htmlToSimpleMarkdown** converteix l'html a un MarkDown molt simple, on només converteix negretes.
- [DateTime] Afegits tres nous helpers:
    - **dateTimeInformation(DateTime date): array**: Desglossa una data en diferents parts, com any, mes, dia, dia de la setmana, dia dins de l'any, hora, minuts i segons-
    - **dateTimeToHumanWriting(DateTime date, bool allDay, string locale): string**: Converteix una data a un format més humà (els idiomes disponibles són Català i Castellà).
    - **dateTimeIntervalToHumanWriting(DateTime start, DateTime end, bool allDay, string locale): string**: Converteix un interval entre dues dates a un format més humà (els idiomes disponibles són Català i Castellà).

1.2.0
-----
- [Email] El mètode **validateEmail** ara es diu **isValid**
- [Email] Solucionat bug al validar un correu, ja que no retornava **true** quan era correcte, nomes false quan era incorrecte.

1.1.2
-----
- [CSV] Ara la generació dels CSV segueix l'estàndard RFC-4180.

1.1.1
-----
- Millorat l'algoritme que genera els uniqIds, ara agafa els últims X valors i no els X primers.

1.1.0
-----
- Afegit un nou Helper per als Emails.
- Solucionat bug amb la configuració del composer.

1.0.0
-----
- Primera versió
